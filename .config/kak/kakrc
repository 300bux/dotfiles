# Line numbers
add-highlighter global/ number-lines -relative -separator "  " -hlcursor

# EditorConfig support
hook global WinCreate ^[^*]+$ %{editorconfig-load}

# kj to escape insert mode
hook global InsertChar j %{ try %{
  exec -draft hH <a-k>kj<ret> d
  exec <esc>
}}

# ctrl backspace delete last word
 map global insert <c-backspace> "<esc><a-b>di"

# Emmet
map global insert <a-e> "<esc>x: emmet<ret>"

# Vertical Selection
map global normal v     ": vertical-selection-down<ret>"
map global normal V     ": vertical-selection-up-and-down<ret>"

# Plugins
source "%val{config}/plugins/plug.kak/rc/plug.kak"
plug "robertmeta/plug.kak" noload
hook global WinSetOption filetype=javascript %{
  set buffer lintcmd 'eslint --config .eslintrc.js --format=node_modules/eslint-formatter-kakoune'
  lint-enable
  lint
}

# Language server
plug "kak-lsp/kak-lsp" do %{
    cargo install --locked --force --path .
} config %{
    # set-option global lsp_cmd "kak-lsp -s %val{session} -vvv --log /tmp/kak-lsp.log"
    declare-option -hidden str lsp_language ""

    set-option global lsp_hover_anchor true
    set-option global lsp_diagnostic_line_error_sign "✗"
    set-option global lsp_diagnostic_line_warning_sign "⚠"

    define-command lsp-hover-info -docstring "show hover info" %{
      set-option buffer lsp_show_hover_format "printf %s "${lsp_info}""
      lsp-hover
    }

    define-command lsp-hover-diagnostics -docstring "show hover diagnostics" %{
      set-option buffer lsp_show_hover_format "printf %s "${lsp_diagnostics}""
      lsp-hover
    }

    define-command lsp-restart -docstring "restart lsp server" %{
        lsp-exit
        lsp-start
    }

    # TODO: would be nice to have <c-space> trigger explicit LSP completion
    # currently kak-lsp does not seem to add entry to <c-x> menu in insert mode

    hook global WinSetOption filetype=(elm|elixir|javascript|typescript|typescriptreact|javascriptreact|python) %{
        echo -debug "initializing lsp for window"
        lsp-enable-window
        set-option window lsp_language %val{hook_param_capture_1}
        map window user ";" ":lsp-hover-info<ret>" -docstring "hover"
        map window user ":" ":lsp-hover-diagnostics<ret>" -docstring "diagnostics"
        map window user . ":lsp-code-actions<ret>" -docstring "code actions"
        map window goto I "\:lsp-implementation<ret>" -docstring "goto implementation"
        map window user <a-h> ":lsp-goto-previous-match<ret>" -docstring "LSP goto previous"
        map window user <a-l> ":lsp-goto-next-match<ret>" -docstring "LSP goto next"
        map window user <a-k> ":lsp-find-error --previous<ret>" -docstring "goto previous LSP error"
        map window user <a-j> ":lsp-find-error<ret>" -docstring "goto next LSP error"
    }
}
plug "JJK96/kakoune-emmet" %{
    hook global WinSetOption filetype=(typescriptreact|javascriptreact|html|css) %{
        emmet-enable-autocomplete
    }
}
plug "occivink/kakoune-snippets"
plug "occivink/kakoune-vertical-selection"
plug "alexherbo2/surround.kak" %{
    map -docstring "Surround" global normal q ": enter-user-mode surround<ret>"
    map -docstring "Surround insert" global normal Q ": surround-enter-insert-mode<ret>"
}
plug "alexherbo2/auto-pairs.kak" %{
    auto-pairs-enable
}

# Add auto-pairs integration
  #map -docstring "Expand snippets" global insert <ret> "<a-;>: snippets-enter auto-pairs-insert-new-line<ret>"

plug "andreyorst/smarttab.kak" defer smarttab %{
    # when `backspace" is pressed, 4 spaces are deleted at once
    set-option global softtabstop 4
} config %{
    # these languages will use `expandtab" behavior
    # hook global WinSetOption filetype=(rust|markdown|kak|lisp|scheme|sh|perl) expandtab
    hook global WinSetOption expandtab
    # these languages will use `noexpandtab" behavior
    hook global WinSetOption filetype=(makefile|gas) noexpandtab
    # these languages will use `smarttab" behavior
    # hook global WinSetOption filetype=(c|cpp) smarttab
    hook global WinSetOption smarttab
}
plug "Delapouite/kakoune-text-objects" %{ 
    text-object-map
}
plug "delapouite/kakoune-auto-percent"
plug "occivink/kakoune-phantom-selection" %{
    map global normal f     ": phantom-selection-add-selection<ret>"
    map global normal F     ": phantom-selection-select-all; phantom-selection-clear<ret>"
    map global normal <a-f> ": phantom-selection-iterate-next<ret>"
    map global normal <a-F> ": phantom-selection-iterate-prev<ret>"
}
plug "delapouite/kakoune-buffers" %{
  map global normal ^ q
  map global normal <a-^> Q
  map global normal q b
  map global normal Q B
  map global normal <a-q> <a-b>
  map global normal <a-Q> <a-B>
  map global normal b ": enter-buffers-mode<ret>" -docstring "buffers"
  map global normal B ": enter-user-mode -lock buffers<ret>" -docstring "buffers (lock)"
}
plug "schemar/kak-jsts"

# Modules
require-module auto-pairs

# kakoune.cr
evaluate-commands %sh{
  kcr init kakoune
}


