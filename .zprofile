#!/bin/zsh

# Adds `~/.local/bin` to $PATH
export PATH="$PATH:$(du "$HOME/.local/bin" | cut -f2 | paste -sd ':')":"$HOME/.npm-global/bin:$PATH"

# Default programs
export EDITOR="kak"
export TERMINAL="st"
export BROWSER="brave"
export READER="zathura"
export OPENER="xdg-open"
export IMAGE="sxiv"

# Clean ~/:
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc-2.0"
export LESSHISTFILE="-"
export WGETRC="${XDG_CONFIG_HOME:-$HOME/.config}/wget/wgetrc"
export INPUTRC="${XDG_CONFIG_HOME:-$HOME/.config}/inputrc"
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"

# nnn
export NNN_FCOLORS="000204020000000000000009"

# .NET
export DOTNET_CLI_TELEMETRY_OPTOUT=true
export DOTNET_NOLOGO=true

# Other
export SUDO_ASKPASS="$HOME/.local/bin/dmenupass"

